## Instalación manual de Metricbeat 7.8.0 
Procedimiento standard que se seguirá para tener la misma configuración

#### Windows

##### Documentación de referencia 

1.- Descargar el instalador:

x64:
https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.8.0-windows-x86_64.zip

x86:
https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.8.0-windows-x86.zip

2.- Descomprimir el contenido en C:/

3.- Cambiar el nombre de la carpeta a "metricbeat"

4.- Ejecutar el siguiente comando
```sh
PS > cd "C:\Program Files\metricbeat"
PS > .\install-service-metricbeat.ps1
// En caso se restrinja el uso del ps1 usar 
PS > PowerShell.exe -ExecutionPolicy UnRestricted -File .\install-service-metricbeat.ps1
```
5.- Alterar los archivos de configuración según la necesidad.

6.- Abrir el administrador de servicios (Start -> services), ubicar el servicio "metricbeat", clic derecho "Start".

7.- Verificar en kibana la existencia del servidor en Metrics.

#### Linux

1.- Descargar el instalador:

##### Debian, Ubuntu y similares x64:
https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.8.0-amd64.deb
##### Centos, RHEL y similares x64:
https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.8.0-x86_64.rpm

2.- Instalar con el siguiente comando:

Debian, Ubuntu y similares x64:
```sh
$ sudo dpkg -i metricbeat-7.8.0-amd64.deb
```

Centos, RHEL y similares x64:
```sh
$ sudo rpm -vi metricbeat-7.8.0-x86_64.rpm
```

3.- Activar mínimo el módulo de sistema:
```sh
$ metricbeat modules enable system
```

4.- Alterar los archivos de configuración según la necesidad:

/etc/metricbeat/metricbeat.yml

/etc/metricbeat/modules.d/system.yml

6.- Iniciar el demonio de Metricbeats
```sh
$ systemctl start metricbeat
```

7.- Verificar en kibana la existencia del servidor en Metrics

8.- Activar el el incio de metricbeat con el sistema operativo
```sh
$ systemctl enable metricbeat
```