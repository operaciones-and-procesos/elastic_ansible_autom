 # Instalacion de servidor Logstash v7.8 en Ubuntu 20.04
 ## Pasos a seguir:
1. Actualizar paquetes instalados:
```sh
sudo apt-get update && sudo apt-get upgrade -y
```
2. Instalar el paquete apt-transport-https:
```sh
   sudo apt-get install apt-transport-https
```
3. Instalar JAVA 14
```sh
   sudo apt install openjdk-14-jdk
```
4. Agregar los PATH al archivo de Ubuntu
```sh
   sudo vi /etc/environment
```
   En el archivo /etc/environment agregar las siguientes variables de entorno:
```sh
JAVA_HOME=/usr/lib/jvm/java-14-openjdk-amd64
LS_HOME=/usr/share/logstash/bin
JRUBY_BIN=/usr/share/logstash/vendor/jruby/bin
RUBYLIB=/usr/share/logstash/lib
```
5. Recargar archivo /etc/environment:
```sh
   source /etc/environment
```
6. Descargar e instalar la clave de firma pública:
```sh
   wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
```
7. Guardar la definición del repositorio en /etc/apt/sources.list.d/elastic-7.x.list:
```sh
   echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
```
8. Instalar el paquete de Logstash
```sh
   sudo apt-get update && sudo apt-get install logstash 
```
9. Ejecutar script startup.options de logstash
```sh
   sudo /usr/share/logstash/bin/system-install	/etc/logstash/startup.options systemd 
```
10. Crear directorio config en logstash y copiar archivo log4j2.properties
```sh
   sudo mkdir /usr/share/logstash/config
   sudo cp /etc/logstash/log4j2.properties /usr/share/logstash/config/log4j2.properties
```
11. En el archivo de los parámetros de la máquina virtual de JAVA que usa Logstash, que se encuentra el archivo jvm.options, se debe agregar:
```sh
   sudo vi /etc/logstash/jvm.options
```
   Insertar después de la línea 7 que contiene, -Xmx1g , en una nueva la línea a continuación, ingresar:
```sh
   -Xss64m  
```
   y después grabar los cambios con ":wq".
13. Crear directorio logs en logstash
```sh
   sudo mkdir /usr/share/logstash/logs
```
14. Cambiar los privilegios a los directorios "data" y "logs"
```sh
   sudo chmod 777 /usr/share/logstash/data
   sudo chmod 777 /usr/share/logstash/logs
```
15. Configurar los pipelines a usar en el primer servidor de logstash, reemplazar contenido del archivo /etc/logstash/pipelines.yml con lo siguiente:
```sh
   sudo vi /etc/logstash/pipelines.yml
```
   Contenido a reemplazar:
```sh
   - pipeline.id: pipeline-1-for-elastic-cloud
     path.config: "/etc/logstash/conf.d/pipeline-1-for-elastic-cloud.conf"
```
16. Configurar los pipelines a usar en el segundo servidor de logstash, reemplazar contenido del archivo /etc/logstash/pipelines.yml con lo siguiente:
```sh
   sudo vi /etc/logstash/pipelines.yml
```
   Contenido a reemplazar:
```sh
   - pipeline.id: pipeline-1-for-elastic-cloud
     path.config: "/etc/logstash/conf.d/pipeline-1-for-elastic-cloud.conf"
```
17. Para el primer servidor de logstash, crear el archivo "pipeline-1-for-elastic-cloud.conf", con el siguiente contenido:
```sh
   sudo vi /etc/logstash/conf.d/pipeline-1-for-elastic-cloud.conf
```
   Y reemplazar el contenido usando el archivo "pipeline-1-for-elastic-cloud.conf"
 ## Configuración segundo servidor
18. Para el segundo servidor de logstash, crear el archivo "pipeline-1-for-elastic-cloud.conf", con el mismo contenido del archivo del primer servidor logstash.
 ## Iniciando el servicio
19. Habilitar logstash para que quede funcionando automático como un servicio, para ello ejecute los siguientes comandos en el primer servidor:
```sh
  sudo systemctl daemon-reload
  sudo systemctl enable logstash
  sudo systemctl start logstash 
```
  Para ver el estado del servicio logstash
```sh
  sudo systemctl status logstash
```
20. Habilitar logstash para que quede funcionando automatico como un servicio, para ello ejecute los siguientes comandos en el segundo servidor:
```sh
  sudo systemctl daemon-reload
  sudo systemctl enable logstash
  sudo systemctl start logstash 
```
  Para ver el estado del servicio logstash
```sh
  sudo systemctl status logstash
```
21. Habilitar reglas de firewall o security groups para abrir los puertos TCP 5044 para el rango de IP 0.0.0.0/0
