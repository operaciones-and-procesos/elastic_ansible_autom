# Instalación Metricbeat en Ubuntu 20.04
Descargar paquete de instalación
```sh
curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.8.0-amd64.deb
```
Instalar paquete
```sh
sudo dpkg -i metricbeat-7.8.0-amd64.deb
```
## Configurar Metricbeat antes de subir servicio:
Activar módulos de los servicios que se van a monitorear, ejemplo:
```sh
sudo metricbeat modules enable system
```
Para obtener los módulos activados o desactivados ejecutar:
```sh
sudo metricbeat modules list
```
Se recomienda hacer un backup del archivo de configuración de Metribeat metricbeat.yml ubicado en /etc/metricbeat:
```sh
sudo cp metricbeat.yml metricbeat.reference.yml
```
Editar metricbeat.yml según destino de las métricas (ElasticSearch, ElasticCloud, Logstash) ejemplos:
  - ElasticCloud
Agregar antes de Modules Configuration para reportar directamente a ElasticCloud y agregar index y metadata, por ejemplo:
```sh
setup.ilm.enabled: true
setup.ilm.rollover_alias: "metricbeat-%{[agent.version]}-dev-linux"
setup.ilm.pattern: "{now/M{yyyy.MM.dd}}-000001"
setup.ilm.policy_name: "metricbeat-dev-linux"
tags: ["dev","orion"]
```
Descomentar en Elastic Cloud cloud.id y cloud.auth y agregar los datos del cloud en paréntesis (conseguir con el administrador)
```sh
cloud.id: "Orion_CCC_Monitoreo:(ingresar ID de EC)"
cloud.auth: "elastic:(ingresar contraseña)"
```
 - Logstash
Descomentar en Logstash Output output.logstash y hosts, así agregar el servidor de Logstash con su respectiva IP y puerto, en el siguiente ejemplo están ambos servicios en el mismo servidor, por lo tanto apunta a localhost.
```sh
output.logstash:
  #The Logstash hosts
  hosts: ["localhost:5044"]
```
Subir el servicio con systemctl
```sh
sudo  systemctl start metricbeat
```
Para revisar estado del servicio, ejecutar:
```sh
sudo  systemctl start metricbeat
```
Activar módulo de AWS
```sh
sudo metricbeat modules enable system
```
Editar el archivo de configuración ubicado /etc/metricbeat/modules.d/
```sh
sudo vi aws.yml
```
Ejemplo:
 - ECS
```sh
- module: aws
  period: 1m
  metricsets:
    - cloudwatch
  metrics:
   - namespace: AWS/ECS
     name: ["CPUUtilization", "MemoryUtilization"]
     access_key_id: ‘ingresar AK’
     secret_access_key:‘ingresar SK'
```
o
 - RDS
```sh
- module: aws
  period: 60s
  metricsets:
    - rds
```
 > Nota: se requiere session token en caso de que el rol sea temporal.
 ## Revisar métricas en ElasticCloud
Ir al menú Discover en la consola web:
Kibana -> Discover -> Filtrar por el index configurado en Logstash
Realizar alguna de estas queries de muestra en KQL y apretar botón verde nombrado “Update”
```sh
not aws.cloudwatch : "" and aws.cloudwatch : *
```
o
```sh
not aws.rds : "" and aws.rds : *
```
