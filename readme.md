## Indice de Documentación
##### [Instalación de Logstash](https://bitbucket.org/operaciones-and-procesos/elastic_ansible_autom/src/65f9d4b26c0fefbd7e7abf77acfa4c6df3a90f7d/manual_install/install_logstash.md?at=master)
##### [Instalación de Metricbeat_Ubuntu](https://bitbucket.org/operaciones-and-procesos/elastic_ansible_autom/src/master/manual_install/install_metricbeat_ubuntu.md)
##### [Instalación de Metricbeat_Windows_Linux](https://bitbucket.org/operaciones-and-procesos/elastic_ansible_autom/src/master/manual_install/install_metricbeat%20_windows_linux.md)
##### [Campos personalizados definidos para el monitoreo]
Se definen los siguientes campos personalizados

->> fields.orion_profile_monitoring
    Valor por defecto: Empty
	Valores admitidos: ["valo1","valor2"] 
	
->> fields.orion_cliente
    Valor no puede estar vacio (not null)
	Valores admitidos: ["Hites","valor2"] 
	
->> fields.orion_mnt_elem
    Valor no puede estar vacio (not null)
	Valores admitidos: ["vm","database"]
	
->> fields.orion_mnt_level: "standard"
    Valor no puede estar vacio (not null)
	Valores admitidos: ["standard","basic","advanced"]
	
->> fields.orion_elem_prfl:
    Valor no puede estar vacio (not null)
	Valores admitidos: ["dflt_mnt_lnx_cpu","dflt_mnt_lnx_memory","dflt_mnt_lnx_disk","dflt_mnt_lnx_swap"]


####### Orion - 2020